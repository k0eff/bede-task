#/bin/bash

# var section
appname=${APP_NAME:-"app"}
maindir=${LOG_DIR:-"/var/log/${appname}/"}

# main config section
importdir="/var/log/${appname}-import/"
date=$(date "+%Y-%m-%d")
filename=${appname}-${date}.log
tempNewFilename=${filename}.temp



# custom logrotate section
mkdir -p $importdir
>> ${maindir}${tempNewFilename} # Elegant and much faster than touch as piping/outputting is a bash built-in. However it still creates an inode, because it needs to write a file. So let's do it in the beginning. ">>" not being ">" is for softness. This approach is better than "echo null" because echo usually adds "\n" and produces a 1-byte file
mv ${maindir}${filename} ${importdir}${filename} # extremely fast
mv -n ${maindir}${tempNewFilename} ${maindir}${filename} # create an empty file for the application in an extremely fast manner - because this command doesn't create inodes. "-n" is to only move it in case destination doesn't exist, i.e. it has already been created by the application. Previous 2 operations are not atomic, but reasonably quick
ls $maindir | grep -vx "$filename" | xargs rm 2>/dev/null # clean up - delete all files except $filename (grep -v) with an exact match (grep -x = regex )  and suppress stderr in case no other files in dir 


# import/process section
# TODO: what to do with these logs?

# cleanup section
# Note: disable this in case of debugging
ls $importdir | xargs rm 2>/dev/null # clean up import dir 

# Debug section

# echo "appname: ${appname}"
# echo "maindir: ${maindir}"
# echo "importdir: ${importdir}"
# echo "date: ${date}"
# echo "filename: ${filename}"
# echo "tempNewFilename: ${tempNewFilename}"
# echo "appname: ${appname}"

# echo "maindir:"
# ls -la $maindir

# echo "importdir:"
# ls -la $importdir
